// Copyright Epic Games, Inc. All Rights Reserved.

#include "unrealSnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, unrealSnakeGame, "unrealSnakeGame" );
